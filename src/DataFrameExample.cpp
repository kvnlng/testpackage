#include <Rcpp.h>
using namespace Rcpp;

// [[Rcpp::export]]
DataFrame DataFrameExample(const DataFrame & DF) {

  IntegerVector a = DF["a"];
  CharacterVector b = DF["b"];
  oldDateVector odv = DF["c"];
  newDateVector ndv = DF["c"];

  a[0] = 42;
  b[0] = "foo";
  odv[0] = odv[0] + 7;
  ndv[0] = ndv[0] + 7;

  DataFrame NDF = DataFrame::create(Named("a") = a,
                                    Named("b") = b,
                                    Named("odv") = odv,
                                    Named("ndv") = ndv);

  return DataFrame::create(Named("origDF") = DF,
                           Named("newDF") = NDF);

}
